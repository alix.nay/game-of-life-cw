import numpy as np

#in this file are all the funcions necessary to compute the next generation of a given universe


#updates the state of a cell of coordinates (col,row) in a given universe for the next generation
#according to the rules of Conway's Game of Life
def survival(row,col,grid,new_grid):
    #calculates how many living neighbors the cell has depending on its position in the grid
    rows = np.size(grid, axis=0)
    cols = np.size(grid, axis=1)
    if row==0 and col==0: #top left corner
        live_neighbors = np.sum(grid[row:row+2, col:col+2]) - grid[row, col]
    elif row==0 and col==cols: #top right corner
        live_neighbors = np.sum(grid[row-1:row+1, row-1:col-1]) - grid[row, col]
    elif row==rows and col==0: #bottom left corner
        live_neighbors = np.sum(grid[row-1:row+1, col:col+2]) - grid[row, col]
    elif row==rows and col==cols: #bottom right corner
        live_neighbors = np.sum(grid[row-1:row+1, col-1:col+1]) - grid[row, col]
    elif row==0 and col!=0: #upmost side of the grid
        live_neighbors = np.sum(grid[row:row+2, col-1:col+2]) - grid[row,col]
    elif row!=0 and col==0: #leftmost side of the grid
        live_neighbors = np.sum(grid[row-1:row+2, col:col+2]) - grid[row, col]
    elif row==rows and col!=cols: #bottommost side of the grid
        live_neighbors = np.sum(grid[row-1:row+1, col-1:col+2]) - grid[row, col]
    elif row!=rows and col==cols: #rightmost side of the grid
        live_neighbors = np.sum(grid[row-1:row+2, col-1:col+1]) - grid[row, col]
    elif row > rows and col > cols: #cell is outside of grid
        raise ValueError("the cell is not in the grid")
    else: #cell has 8 neighbors
        live_neighbors = np.sum(grid[row-1:row+2, col-1:col+2]) - grid[row, col]
    
    #applies the rules of Conway's game and updates the state of the cell in a new grid
    if grid[row, col] == 1: # The cell is alive
        if live_neighbors < 2: # It dies because there are less than 2 live neighbors
            new_grid[row, col] = 0
        elif live_neighbors > 3: # It dies because there are more than 3 live neighbors
            new_grid[row, col] = 0
        else: # It remains alive because there are either 2 or 3 live neighbors
            new_grid[row, col] = 1
    else: # The cell is dead
        if live_neighbors == 3: # It becomes alive because there are exactly 3 live neighbors
            new_grid[row, col] = 1
        
#the following lines are for testing purposes
    #return new_grid
#array = np.array([[1, 0, 0, 1],
#                 [1, 1, 1, 0],
#                 [0, 1, 0, 0],
#                 [0, 1, 0, 1]])

#rows, cols = array.shape
#new_array = np.zeros((rows, cols), dtype=int)
   
#x, y = 2, 2  # Example coordinates
#print(survival(y, x, array, new_array))


#updates the state of each cell in an entire universe
def generation(grid):
    rows, cols = grid.shape
    new_grid = np.zeros((rows, cols), dtype=int)
    for row in range(rows):
       for col in range(cols):
           survival(row,col,grid,new_grid)
    return new_grid


#the following lines are for testing purposes
#array = np.array([[1, 0, 0, 1],
#                 [1, 1, 1, 0],
#                 [0, 1, 0, 0],
#                 [0, 1, 0, 1]])

#print(generation(array))

#gives the n-th generation of a given universe where n=iteration
#this function is not used in the final animation
def game_life_simulate(grid, iteration):
    for i in range(iteration):
        current_grid = generation(grid)
    return current_grid