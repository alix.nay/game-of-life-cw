# game of life-CW 

## READ ME

### Table of content 
1. [How to use](#how-to-use)
2. [Credits](#credits)

### How to use 
To launch the game of life, please run the [main.py](main.py) file. This text should appear in your terminal:
```
Welcome to the game of life! 

 We propose 3 options on how to run the game:

 1) Run the game by inputting your starting conditions in the terminal as they are requested of you. In order to do this, type 1. 
 2) Open a graphical interface to input your starting conditions. In order to do this, type 2. 
 3) Close this prompt and run a command in your terminal. The command should look like this: 
          python argparse_feature.py 'seed' dim_x dim_y start_x start_y 
          with 'seed' the name of a seed
          dim_x and dim_y positive integers describing the dimensions of the starting universe
          start_x and start_y positive integers describing the starting coordinates of the top left corner of the chosen seed.

 You may close this prompt by typing 'close'. 
 You may access the list of available seeds by typing 'seeds'.
How would you like to run the game? Type 1, 2, seeds or close: 
``` 

You can chose to stay in the terminal or go to another window to choose the game's parameters.

To stay in the terminal, please choose:
- **1** : you will be directly asked in the terminal to input your start parameters through questions.
    
    You will get a list of seeds to choose from. To select a seed, simply type its name without **' '**. You will then have to choose the size of the universe. If you don't enter correct coordinates, you will be asked to enter new ones. You will also have to enter the start position of your seed. If your size universe is too small compared to your seed size, you will be asked to enter new values until you enter valid ones. If your starting position is invalid given your seed and universe size, you will be asked to enter a new one.

- **3** : you will have to close the terminal by typing 'close' without ' ' and write the command 

```
python argparse_feature.py 'Seed' dim_x dim_y start_x start_y
```
with  `dim_x` and `dim_y` the size of the universe and `start_x` and `start_y` the start position of the seed. They should all be positive integers.

To go to another window, please choose:
- **2** : you will have to input your game parameters in the indicated boxes and choose your seed. Once this is done, click on `Enter data`. If you chose a universe size smaller than the seed's, you will get an error message. To re-enter your data, simply wait for the error message window to close on itself.

---



### Credits 
This project was done by:
- Joseph Rozan 
- Dominique Kentsa 
- Typhaine Braucourt 
- Alexis Stylemans 
- Alix Nay 