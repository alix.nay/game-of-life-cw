import numpy as np
from seeds import seeds

#in this file are all the functions necessary to initialize the game
#while making sure the user's inputs are valid at all times.

#generates blank universe (array of zeros) of dimensions (x,y)
def generate_universe(x,y):
        return np.zeros((x,y))

#checks if a string input is a natural integer or not
def is_positive_integer(input_str):
    try:
        num = int(input_str)
        if num >= 0:
            return True
        else:
            return False
    except ValueError:
        return False
    
#checks whether the universe is bigger than the seed chosen by the user
def check_size(universe,seed):
    seed = np.array(seed)
    x_seed = seed.shape[1]
    y_seed = seed.shape[0]
    x_uni = universe.shape[1]
    y_uni = universe.shape[0]
    if x_uni < x_seed or y_uni < y_seed:
        return False
    else:
        return True    

#adds the seed at a random position on a predefined universe
def add_seed_random(universe,seed): 
    seed = np.array(seed)
    start_x = np.random.randint(0, universe.shape[0] - seed.shape[0] + 1)
    start_y = np.random.randint(0, universe.shape[1] - seed.shape[1] + 1)
    universe[start_x:start_x + seed.shape[0], start_y:start_y + seed.shape[1]] += seed
    return universe

#checks if a given starting position is legitimate (the seed will not go out of bounds) given a seed and universe
def check_seed(universe,seed,x_start,y_start):
    seed = np.array(seed)
    x_uni = universe.shape[1]
    y_uni = universe.shape[0]
    x_seed = seed.shape[1]
    y_seed = seed.shape[0]
    if x_start >= (x_uni - x_seed +1) or y_start >= (y_uni - y_seed + 1): 
        return False
    else:
        return True

#adds a seed at a given starting position on a predefined universe
def add_seed(grid,seed,x_start,y_start): 
    seed = np.array(seed)
    grid[x_start:x_start + seed.shape[0], y_start:y_start + seed.shape[1]] += seed
    return grid

#recurring messages in the later functions, defined as variables to clean up the code
ask_uni_x = "\nWhat will the horizontal dimension of the universe be? Provide a positive integer:  "
ask_uni_y = "\nWhat will the vertical dimension of the universe be? Provide a positive integer:  "
int_error = "\nYou did not input a positive integer, please input a new number."
ask_startpos_x = "\nWhat will the horizontal starting coordinate of the seed be? (coordinates (0,0) are the top left corner) Provide a positive integer:  "
ask_startpos_y = "\nWhat will the vertical starting coordinate of the seed be? (coordinates (0,0) are the top left corner) Provide a positive integer:  "

#recurring checks made at multiple steps to ensure the inputs are positive integers, made into a function to clean up the code
def get_universe_dimensions():
    universe_size_x = input(ask_uni_x)
    while is_positive_integer(universe_size_x) == False:
        print(int_error)
        universe_size_x = input(ask_uni_x)
    universe_size_y = input(ask_uni_y)
    while is_positive_integer(universe_size_y) == False:
        print(int_error)
        universe_size_y = input(ask_uni_y)
    return [universe_size_x, universe_size_y]

def get_startpos_coordinates():
    startpos_x = input(ask_startpos_x)
    while is_positive_integer(startpos_x) == False:
        print(int_error)
        startpos_x = input(ask_startpos_x)
    startpos_y = input(ask_startpos_y)
    while is_positive_integer(startpos_y) == False:
        print(int_error)
        startpos_y = input(ask_startpos_y)
    return [startpos_x, startpos_y]

#initializes the grid (returns 'generation 0', or places a chosen seed in a chosen universe at a chosen position) 
#and prompts the user for new inputs if the ones they choose are invalid
def initialize_game_main():
    print("\nBegin by choosing the starting conditions for the game.")
    #Lets the user choose a seed
    print("\nHere is the list of available seeds: "  , list(seeds.keys()))
    seed_input = input("\nChoose a seed:  ")
    #Checks if the seed is in the list of proposed seeds, if not, keeps asking until a valid seed is entered
    while seed_input not in seeds.keys():
        print("\nThe chosen seed is not in the list of seeds. Make sure the spelling and capitalization are correct.")
        seed_input = input("\nChoose a seed:  ")
    seed = seeds[seed_input]
    #Lets the user choose the size of the universe and checks if they are positive integers
    universe_dimensions = get_universe_dimensions()
    #Creates an empty universe once the dimensions are validated
    universe = generate_universe(int(universe_dimensions[0]),int(universe_dimensions[1]))
    #Checks if the universe is bigger than the chosen seed, if not, asks for new universe size until a valid one is entered
    while check_size(universe, seed) == False:
        print("\nThe seed size exceeds the size of the universe, please input a new universe size")
        universe_dimensions = get_universe_dimensions()
        universe = generate_universe(int(universe_dimensions[0]),int(universe_dimensions[1]))
    #Lets the user choose the starting position of the seed and checks if they are positive integers
    start_pos = get_startpos_coordinates()
    #Checks if the start position is legitimate given the seed and size of the universe
    while check_seed(universe, seed, int(start_pos[0]), int(start_pos[1])) == False:
        print("\nThe seed will go out of bounds of the universe at the given starting coordinates. Provide new coordinates.")
        start_pos = get_startpos_coordinates()
    #adds the seed to the universe once the start position is validated
    universe = add_seed(universe, seed, int(start_pos[0]), int(start_pos[1]))
    return universe

#a tweaked version of the above function to use with argparse
def initialize_game_argparse(uni_x, uni_y, start_x, start_y, seed):
    if seed not in seeds.keys():
        return 'The seed you have chosen is not in the list of available seeds. As a reminder, here is the list of seeds: Boat, R_pentomino, Beacon, Acorn, Block_switch_engine, Infinite, Mango, Blinker, Bee-hive, Loaf, Tub, Diehard, Angel, Toad, Anvil, Quadpole, Achimps16, Tumbler, Thunderbird, Ants, Beetle, Sun, Frogs, Grapler, Typh, Alix'
    else:
        seed = seeds[seed]
        if is_positive_integer(uni_x) == False or is_positive_integer(uni_y) == False or is_positive_integer(start_x) == False or is_positive_integer(start_y) == False:
            return "You did not input positive integers where needed. Run the command again with positive integers in the correct arguments."
        else: 
            if check_size(generate_universe(int(uni_x),int(uni_y)), seed) == False:
                return 'The size of your seed if bigger than the size of your universe. Run the command again and increase the size of the universe or choose another seed.'
            elif check_seed(generate_universe(int(uni_x),int(uni_y)), seed, int(start_x), int(start_y)) == False:
                return "The seed will go out of bounds of the universe at the given starting coordinates. Run the command again with new coordinates."
            else: 
                universe = add_seed(generate_universe(uni_x,uni_y), seed, int(start_x), int(start_y))
                return universe
            
            
#a tweaked version of initialize_game_main for the tkinter function
def initialize_game_tkinter(input_tuple):
    seed = seeds[input_tuple[2]]
    universe = add_seed(generate_universe(int(input_tuple[0]),int(input_tuple[1])), seed, int(input_tuple[3]), int(input_tuple[4]))
    return universe
