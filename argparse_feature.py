from initialize_game import * 
from visualize_game import * 
from seeds import seeds
import argparse 


#in this file is all the code necessary to be able to play the Game of Life by using a terminal command
#the command is of the form: python argparse_feature.py 'seed_name' universe_size_x universe_size_y start_pos_x start_pos_y

parser = argparse.ArgumentParser(description='Please choose a seed, your universe size and a start point')

parser.add_argument('seed', type=str, help='Please choose a seed in the following list: Boat, R_pentomino, Beacon, Acorn, Block_switch_engine, Infinite, Mango, Blinker, Bee-hive, Loaf, Tub, Diehard, Angel, Toad, Anvil, Quadpole, Achimps16, Tumbler, Thunderbird, Ants, Beetle, Sun, Frogs, Grapler, Typh, Alix')
parser.add_argument('x_universe', type=int, help="Choose the horizontal dimension of the universe. Provide a positive integer")
parser.add_argument('y_universe', type=int, help="Choose the vertical dimension of the universe. Provide a positive integer")
parser.add_argument('x_start_point', type=int, help="Choose the horizontal starting coordinate of the seed. (coordinates (0,0) are the top left corner) Provide a positive integer")
parser.add_argument('y_start_point', type=int, help="Choose the vertical starting coordinate of the seed. (coordinates (0,0) are the top left corner) Provide a positive integer")
args = parser.parse_args()

animate_game_of_life(initialize_game_argparse(args.x_universe,args.y_universe,args.x_start_point,args.y_start_point,args.seed))

