import matplotlib.pyplot as plt
import matplotlib.animation as animation
from evolution import *
from initialize_game import *

#in this file are all the functions necessary to visualize the state of a given universe and 
#fully animate the game of life

#creates a visual representation of the grid
def visualize_grid(grid):
    grid = np.array(grid)  
    grid = 1 - grid
    plt.imshow(grid, cmap='gray', interpolation='none')  
    
    plt.grid(which='major', axis='both', linestyle='-', color='k', linewidth=0.5)
    plt.xticks(np.arange(-.5, len(grid[0]), 1))
    plt.yticks(np.arange(-.5, len(grid), 1))
    
    plt.tick_params(axis='both', which='both', labelbottom=False, labelleft=False)
    plt.show()

#animates the game fully given a starting universe




def animate_game_of_life(universe):
    # Set up the plot
    fig, ax = plt.subplots()
    img = ax.imshow(universe, cmap='binary', interpolation='nearest')

    # Update function for animation
    def update(num_iterations):
        nonlocal universe
        universe = generation(universe)
        img.set_array(universe)
        return img,

    # Set the grid to delineate the cells
    ax.set_xticks(np.arange(-.5, universe.shape[1], 1), minor=True)
    ax.set_yticks(np.arange(-.5, universe.shape[0], 1), minor=True)
    ax.grid(which='minor', color='k', linestyle='-', linewidth=0.5)

    # Hide the major tick marks
    ax.tick_params(axis='both', which='major', labelbottom=False, labelleft=False)

    # Create the animation
    ani = animation.FuncAnimation(fig, update, frames=100, interval=150, blit=False)

    # Display the plot
    plt.show()



