from initialize_game import *
from seeds import seeds
from visualize_game import *
from tkinter_feature import *


#this is the main file, which is run in order to access all the features of our project

#allows the user to run the game of life by inputting the demanded values in the terminal
def game_of_life_terminal():
    universe = initialize_game_main()
    animate_game_of_life(universe)
    
#the main function, allows user to run the game in the terminal or through tkinter
def main():
    print("\nWelcome to the game of life! \n\n We propose 3 options on how to run the game:\n\n 1) Run the game by inputting your starting conditions in the terminal as they are requested of you. In order to do this, type 1. \n 2) Open a graphical interface to input your starting conditions. In order to do this, type 2. \n 3) Close this prompt and run a command in your terminal. The command should look like this: \n          python argparse_feature.py 'seed' dim_x dim_y start_x start_y \n          with 'seed' the name of a seed\n          dim_x and dim_y positive integers describing the dimensions of the starting universe\n          start_x and start_y positive integers describing the starting coordinates of the top left corner of the chosen seed.\n\n You may close this prompt by typing 'close'. \n You may access the list of available seeds by typing 'seeds'.")
    while True:
        user_input = input("How would you like to run the game? Type 1, 2, seeds or close:  ")
        if user_input == '1':
            game_of_life_terminal()
            break
        elif user_input == '2':
            print("A graphical interface window should have opened on your computer.")
            game_of_life_tkinter()
            break
        elif user_input == 'seeds':
            print("\nHere is our list of seeds:  " , list(seeds.keys()) , "\n")
        elif user_input == 'close':
            return
        else:
            print("It seems you mispelled an option, type 1, 2, seeds or close with no quotation marks around them.")
        
if __name__ == "__main__":
    main()
