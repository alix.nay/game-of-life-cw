import tkinter as tk
from tkinter import ttk
from seeds import *
from initialize_game import *
from evolution import *
from time import time
#in this file are all the functions necessary to allow users to run the game of life in a graphical interface using tkinter

#given all the information necessary to run the game as a tuple, runs the game of life in a graphical interface
def tkinter_function(input_tuple):
    
    universe = initialize_game_tkinter(input_tuple)

    # Create the main window
    root = tk.Tk()
    root.title("Conway's Game of Life")

    # Set the size of each cell
    cell_size = 15

    # Create a canvas widget
    canvas = tk.Canvas(root, width=universe.shape[1]*cell_size, height=universe.shape[0]*cell_size)
    canvas.pack()

    # Function to draw the universe on the canvas
    def draw_universe():
        canvas.delete("all")  # Clear the canvas
        for i, row in enumerate(universe):
            for j, cell in enumerate(row):
                color = "black" if cell else "white"
                x1 = j * cell_size
                y1 = i * cell_size
                x2 = x1 + cell_size
                y2 = y1 + cell_size
                canvas.create_rectangle(x1, y1, x2, y2, fill=color, outline="gray")

    # Function to update the universe
    def update():
        nonlocal universe
        universe = generation(universe)
        draw_universe()
        root.after(200, update)  # Schedule the next update

    # Start the animation
    update()

    # Start the GUI event loop
    root.mainloop()


#creates a graphical interface for the user to input values in different fields, then returns those values as a tuple
def interface_coordinates():
    # Assuming seeds is a dictionary that exists
    # seeds = {'Seed1': value1, 'Seed2': value2, ...}
    # Dictionary to store the values
    values = {}

    def enter_data():
        # Retrieve the user's input and store in the dictionary
        values['X_universe'] = X_universe_entry.get()
        values['Y_universe'] = Y_universe_entry.get()
        values['Starting_position_X'] = Starting_position_X_entry.get()
        values['Starting_position_Y'] = Starting_position_Y_entry.get()
        values['seed_universe'] = combobox.get()
        # Close the window after getting the input
        window.destroy()

    # ... (rest of your code remains unchanged)

    # Start the GUI event loop and wait for the user to enter data
    
    # After the window is closed, return the values from the dictionary


    # Create the main window
    window = tk.Tk()
    window.title("Choose Your Parameters")

    # Create a frame to contain the widgets
    frame = tk.Frame(window)
    frame.pack()

    # Create a label frame to contain the universe size inputs
    size_frame = tk.LabelFrame(frame, text="Universe Size and Seed")
    size_frame.grid(row=0, column=0, padx=40, pady=0)
    seed_choice_frame=tk.LabelFrame(frame,text='Seed')
    seed_choice_frame.grid(row=1,column=0, padx=25, pady=0)
    Starting_position= tk.LabelFrame(frame, text="Starting Position")
    Starting_position.grid(row=2, column=0, padx=30, pady=0)
    # Create labels and entry widgets for the X and Y universe sizes
    Starting_position_X_Label=tk.Label(Starting_position,text="Horizontal Start Position")
    Starting_position_X_Label.grid(row=0,column=0)
    Starting_position_Y_Label=tk.Label(Starting_position,text="Vertical Start Position")
    Starting_position_Y_Label.grid(row=0,column=1)
    X_universe_label = tk.Label(size_frame, text="X Universe")
    X_universe_label.grid(row=0, column=0)
    Y_universe_label = tk.Label(size_frame, text="Y Universe")
    Y_universe_label.grid(row=0, column=1)

    X_universe_entry = tk.Entry(size_frame)
    Y_universe_entry = tk.Entry(size_frame)
    X_universe_entry.grid(row=1, column=0)
    Y_universe_entry.grid(row=1, column=1)
    Starting_position_X_entry=tk.Entry(Starting_position)
    Starting_position_Y_entry=tk.Entry(Starting_position)
    Starting_position_X_entry.grid(row=1,column=0)
    Starting_position_Y_entry.grid(row=1,column=1)
    # Assuming seeds is a dictionary that exists
    OptionList = list(seeds.keys())
    combobox = ttk.Combobox(seed_choice_frame, values=OptionList)
    combobox.grid(row=2, column=0, columnspan=2)  # Span both columns

    # Create a button to submit the data
    button = tk.Button(frame, text='Enter Data', command=enter_data)
    button.grid(row=4, column=0, padx=20, pady=20)


    # Start the GUI event loop and wait for the user to enter data
    window.mainloop()

    # After the window is closed, return the values

    return ((values['X_universe']), (values['Y_universe']), values['seed_universe'],(values['Starting_position_X']), (values['Starting_position_Y']))


#function to create an error window with a custom message, is used in the graphical interface
def error_window(error):
    """A function to create an error message box"""
    root = tk.Tk()
    root.title("Error Message")
    label = tk.Label(root, text=error)
    start=time()
    root.after(5000,root.destroy)
    label.pack(pady=50)
    root.mainloop()

    
#function to check whether the inputs of the user pass all the necessary checks for the game to run properly, if not, identifies which error type it is.
def passes_checks(input_tuple):
    if input_tuple[2] not in seeds.keys():
        return 'seederror'
    try:
        converted_tuple = (int(input_tuple[0]), int(input_tuple[1]), int(input_tuple[3]), int(input_tuple[4]))
        seed = seeds[input_tuple[2]]
        if converted_tuple[0] < 0 or converted_tuple[1] < 0 or converted_tuple[2] < 0 or converted_tuple[3] < 0:
            return 'interror'        
        elif check_size(generate_universe(converted_tuple[0],converted_tuple[1]), seed) == True :
            if check_seed(generate_universe(converted_tuple[0],converted_tuple[1]), seed, converted_tuple[2], converted_tuple[3]) == True:
                return True
            else: return 'checkseederror'
        else: 
            return 'checksizeerror'
    except ValueError:
        return 'interror'

#launches an interface asking for user input, then returns an error message if necessary, and lauches the game otherwise
def game_of_life_tkinter():
    while True:
        parameters = interface_coordinates()
        if passes_checks(parameters) == True:
            tkinter_function(parameters)
            break
        elif passes_checks(parameters) == 'seederror':
            error_window("The chosen seed is not in the list of seeds. Make sure the spelling and capitalization are correct or use the drop-down to select.")
        elif passes_checks(parameters) == 'interror':
            error_window("You did not input positive integers for the universe size or starting coordinates, please input new numbers.")
        elif passes_checks(parameters) == 'checksizeerror':
            error_window("The size of your seed if bigger than the size of your universe. increase the size of the universe or choose another seed.")
        elif passes_checks(parameters) == 'checkseederror':
            error_window("The seed will go out of bounds of the universe at the given starting coordinates. Run the command again with new coordinates.")
        else: 
            error_window("There is an error somewhere in your inputs. \n\n The possible errors are: \n 1) You did not input a positive integer in the 'X Universe', 'Y Universe', 'Horizontal Start Position' or 'Vertical Start Position' fields. \n 2) The seed you have chosen is bigger than your created universe. In this case, input bigger dimensions for the universe. \n 3) Your starting position causes your seed to go out of bounds, either choose another seed or create a bigger universe. \n\n Please try updating your inputs")



